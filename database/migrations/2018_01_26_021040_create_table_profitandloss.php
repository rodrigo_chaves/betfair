<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProfitandloss extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profitandloss',function(Blueprint $table){
            $table->increments('id');
            $table->string('market')->nullable();
            $table->datetime('start_time');
            $table->datetime('resolution_date');
            $table->decimal('profit_prejudice',2,2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profitandloss');
    }
}
