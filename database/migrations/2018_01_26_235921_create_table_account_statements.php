<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAccountStatements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_statements',function(Blueprint $table){
            $table->increments('id');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('account_statement_types_id')->unsigned();
            $table->foreign('account_statement_types_id')->references('id')->on('account_statement_types');

            $table->string('ref_id');
            $table->datetime('placed');
            $table->string('description');
            $table->decimal('average_odds',3,2);
            $table->decimal('stopped',3,2);
            $table->string('state');
            $table->decimal('rd',3,2);
            $table->decimal('rc',3,2);
            $table->decimal('balance',8,2);

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('account_statements');
    }
}
