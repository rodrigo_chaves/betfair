<?php

use Illuminate\Database\Seeder;

use App\Model\AccountStatementType;

class AccountTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $accountStatementType = new AccountStatementType;
        $accountStatementType->description = 'exge';
        $accountStatementType->save();

        $accountStatementType = new AccountStatementType;
        $accountStatementType->description = 'pr';
        $accountStatementType->save();

        $accountStatementType = new AccountStatementType;
        $accountStatementType->description = 'gamex';
        $accountStatementType->save();

        $accountStatementType = new AccountStatementType;
        $accountStatementType->description = 'arcade';
        $accountStatementType->save();

        $accountStatementType = new AccountStatementType;
        $accountStatementType->description = 'SKILLDICE';
        $accountStatementType->save();

        $accountStatementType = new AccountStatementType;
        $accountStatementType->description = 'dpw';
        $accountStatementType->save();

        $accountStatementType = new AccountStatementType;
        $accountStatementType->description = 'drc';
        $accountStatementType->save();
    }
}
