<?php

use Illuminate\Database\Seeder;
use App\Model\Balance;
use App\Model\User;

class CreateUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'Rodrigo';
        $user->email = 'chaves_dev@hotmail.com';
        $user->password = '123456';

        $user->save();

        $balance = new Balance();
        $balance->user_id = $user->id;
        $balance->save();
    }
}
