@extends('layout.panel')

@section('content')
<div id="main" role="main">
    <div id="mod-news-banner-77251-container" class="mod">
        <div id="mod-news-banner-77251" class="mod-news-banner mod-news-banner-news-banner">
        </div>
    </div>
    <div id="mod-kyc-banner-77252-container" class="mod yui3-widget yui3-module yui3-kyc-banner">
        <div id="mod-kyc-banner-77252" class="mod-kyc-banner mod-kyc-banner-kyc-banner yui3-kyc-banner-content">
            <div class="mod-kyc-banner">
            </div>
        </div>
    </div>
    <div id="mod-summary-balance-77253-container" class="mod yui3-widget yui3-module yui3-summary-balance">
        <div id="mod-summary-balance-77253" class="mod-summary-balance mod-summary-balance-summary-balance mod-summary-balance mod-summary-balance-summary-balance ga yui3-summary-balance-content">
            <div class="mod-summary-balance-section">
                <div class="summary-balance-content">
                    <div data-view="expanded" id="expandedView" class="">
                        <div id="top-panel-container">
                            <div class="top-panel maw-header-text maw-header-text--main">
                                <h4 class="balances-title" data-hj-masked=""><strong>Olá, Augusto,</strong> aqui está quanto podes apostar:</h4>
                                <a class="maw-header-text__link e" href="#" id="hideBalances">
                                Ocultar saldos</a>
                            </div>
                        </div>
                        <div class="wallets-container" id="yui_3_3_0_1_1516824388685563">
                            <div class="wallets-funds">
                                <div class="availableToBet refreshTransition">
                                    <label class="amount r-availableToBet">
                                    {{number_format($user->balance->master,2)}}
                                    </label>
                                    <span class="refresh-icon"></span>
                                    <div class="manage--funds">
                                        <div class="display-type">
                                            <span id="simple-view" class="active">Vista simples</span><span id="detailed-view">Vista detalhada</span>
                                        </div>
                                        <div class="funds-buttons">
                                            <button type="button" id="toggle--transfer--funds" class="btn btn--default" href="#" data-event="expand">Transferir fundos</button>
                                            <a class="btn btn--default" href="https://myaccount.betfair.com/payments/withdraw" id="withdrawFunds">Levantar fundos</a>
                                            <a class="btn btn--primary" href="https://myaccount.betfair.com/payments/deposit" target="_parent" name="depositFunds" id="depositFunds">Depositar fundos</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="transfer--funds refreshTransition" data-visible="false">
                                    <ul class="transfer-form">
                                        <li>
                                            <label for="transfer-amount">Valor da transferência (€)</label>
                                            <div class="transfer-amount-container">
                                                <input name="transfer-amount" id="transfer-amount" maxlength="10" placeholder="€" type="text">
                                                <div class="amount-tooltip hide">
                                                    Valor da transferência: <span class="amount-currency-code">€</span><span class="transfer-amount-parsed">0.00</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <label for="transfer-from">da</label>
                                            <div class="fancy-select" data-rel="transfer-from">
                                                <select id="transfer-from" data-fancy-select="on" class="s-hidden">
                                                    <option value="MAIN">Carteira principal (€26.75)</option>
                                                    <option value="XG">Carteira Jogos Exchange (€0.61)</option>
                                                    <option value="POKER">Carteira da Betfair Poker (€0.08)</option>
                                                </select>
                                                <div class="styledSelect" style="font-size: 12px;">Carteira principal (€26.75)</div>
                                                <ul class="options" id="yui_3_3_0_1_1516824388685604" style="display: none;">
                                                    <li tabindex="0" rel="MAIN">Carteira principal (€26.75)</li>
                                                    <li tabindex="1" rel="XG">Carteira Jogos Exchange (€0.61)</li>
                                                    <li tabindex="2" rel="POKER">Carteira da Betfair Poker (€0.08)</li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li>
                                            <label for="transfer-to">para</label>
                                            <div class="fancy-select" data-rel="transfer-to">
                                                <select id="transfer-to" data-fancy-select="on" class="s-hidden">
                                                    <option value="empty">Seleciona</option>
                                                    <option value="MAIN">Carteira principal (€26.75)</option>
                                                    <option value="XG">Carteira Jogos Exchange (€0.61)</option>
                                                    <option value="POKER">Carteira da Betfair Poker (€0.08)</option>
                                                </select>
                                                <div class="styledSelect" style="font-size: 12px;">Seleciona</div>
                                                <ul class="options" id="yui_3_3_0_1_1516824388685660" style="display: none;">
                                                    <li tabindex="0" rel="empty">Seleciona</li>
                                                    <li tabindex="1" rel="MAIN">Carteira principal (€26.75)</li>
                                                    <li tabindex="2" rel="XG">Carteira Jogos Exchange (€0.61)</li>
                                                    <li tabindex="3" rel="POKER">Carteira da Betfair Poker (€0.08)</li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="last">
                                            <button class="btn btn--primary" disabled="disabled" id="make-transfer">Transferir</button>
                                        </li>
                                    </ul>
                                    <div class="transfer--funds--messages ">
                                    </div>
                                    <span class="spinner svg-loader svg-loader--color " style="width:24px; height:24px;"></span>
                                </div>
                                <div class="main-wallets-section refreshTransition">
                                    <div class="maw-header-text maw-header-text--second" id="main-wallets-header">
                                        <h4>Saldos em dinheiro</h4>
                                    </div>
                                    <div class="main-wallets-container">
                                        <table id="main-wallets">
                                            <tbody>
                                                <tr>
                                                    <td class="money bold Carteira principal r-mainWallet">{{number_format($user->balance->master,2)}}</td>
                                                    <td><label>Carteira principal</label></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td class="money bold Carteira Jogos Exchange r-gamesWallet">€100,000.00</td>
                                                    <td><label>Carteira Jogos Exchange</label></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td class="money bold Carteira da Betfair Poker r-pokerWallet">€0.00</td>
                                                    <td><label>Carteira da Betfair Poker</label></td>
                                                    <td></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <span class="spinner svg-loader svg-loader--color " style="width:24px; height:24px;"></span>
                                </div>
                            </div>
                            <div class="row promotion-points-section cf" id="yui_3_3_0_1_1516824388685572">
                                <div class="yourPoints full-width">
                                    <div class="maw-header-text maw-header-text--second">
                                        <h4>Os teus pontos</h4>
                                        <a class="maw-header-text__button" href="https://myaccount.betfair.com/rewards/points-and-holidays" target="_parent">Mais</a>
                                    </div>
                                    <div class="points">
                                        <div class="points-inside clearfix">
                                            <div class="uber-text">34</div>
                                            <div class="label">Pontos Exchange</div>
                                            <div class="uber-text">&nbsp;=&nbsp;0%</div>
                                            <div class="label">Desconto de comissões</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <span class="spinner svg-loader svg-loader--color " style="width:108px; height:108px;"></span>
                    </div>
                    <div data-view="compressed" class="hidden">
                        <div class="top-panel maw-header-text maw-header-text--main">
                            <h4 class="balances-title" data-hj-masked=""><strong>Olá, Augusto,</strong> aqui está quanto podes apostar:</h4>
                            <a class="maw-header-text__link" href="#" id="showBalances">Mostrar saldos</a>
                        </div>
                    </div>
                    <div class="hidden">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="mod-mybets-min-77254-container" class="mod yui3-widget yui3-module yui3-mybetsmin">
        <div id="mod-mybets-min-77254" class="mod-mybets-min mod-mybets-min-mybets-min yui3-mybetsmin-content">
            <div class="iframe-container">
                <iframe id="iframe-minbets" seamless="seamless" scrolling="no" src="https://myactivity.betfair.com/#/exchange-min" style="height: 149px;"></iframe>
            </div>
        </div>
    </div>
    <div id="mod-summary-usage-reporting-77255-container" class="mod yui3-widget yui3-module yui3-summary-usage-reporting">
        <div id="mod-summary-usage-reporting-77255" class="mod-summary-usage-reporting mod-summary-usage-reporting-summary-usage-reporting yui3-summary-usage-reporting-content">
            <div class="mod-summary-usage-reporting">
                <div class="mybets-links">
                    <label>Mostrar-me as minhas apostas a favor:</label>
                    <a href="https://casino.betfair.com/cashier" data-ga="Casino" target="_blank" data-qa="Casino"> Casino </a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-group">
        <div class="col-mb-2 col-mb-box tooltips-right">
        </div>
        <div class="col-mb-2 col-mb-box tooltips-left">
        </div>
    </div>
    <div id="mod-footer-widgets-77260-container" class="mod yui3-widget yui3-module yui3-bfmodule-footer-widgets">
        <div id="mod-footer-widgets-77260" class="mod-footer-widgets mod-footer-widgets-footer-widgets yui3-bfmodule-footer-widgets-content">
            <div class="wrapper clearfix">
                <div id="left-column">
                    <div class="maw-header-text maw-header-text--main">
                        <h4>Principais questões sobre A minha conta</h4>
                    </div>
                    <div id="helpWidget" class="">
                        <div id="error"></div>
                        <div id="top-questions-widget">
                            <ul class="rn_List">
                                <li class="rn_Item">
                                    <a class="rn_Link" target="_blank" href="https://pt-betfair.custhelp.com/app/answers/detail/a_id/2511">Intercâmbio: O que é a taxa de desconto?</a>
                                </li>
                                <li class="rn_Item">
                                    <a class="rn_Link" target="_blank" href="https://pt-betfair.custhelp.com/app/answers/detail/a_id/2896">Autenticação de dois passos – O que é e porque devo utilizá-la?</a>
                                </li>
                                <li class="rn_Item">
                                    <a class="rn_Link" target="_blank" href="https://pt-betfair.custhelp.com/app/answers/detail/a_id/6412/c/456">Sportsbook: Bonus Perguntas Frequentes</a>
                                </li>
                                <li class="rn_Item">
                                    <a class="rn_Link" target="_blank" href="https://pt-betfair.custhelp.com/app/answers/detail/a_id/2540">Intercâmbio: O que é a taxa base do mercado?</a>
                                </li>
                                <li class="rn_Item">
                                    <a class="rn_Link" target="_blank" href="https://pt-betfair.custhelp.com/app/answers/detail/a_id/2887">Mobile: O que é a autenticação com base em PIN?</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection