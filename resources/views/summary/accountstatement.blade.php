@extends('layout.panel')

@section('content')

<div id="main" role="main">
  <div id="mod-iframe-393562-container" class="mod yui3-widget yui3-module yui3-bfmodule-iframe">
    <div id="mod-iframe-393562" class="mod-iframe mod-iframe-iframe yui3-bfmodule-iframe-content">
      <script type="text/javascript">
          window.exchangeLoaded = function (id) {};

          window.getHelp = function (sKey, sAnchor) {
              window.spawn('help', sKey, sAnchor);
          };

          window.logoutButtonAction = function () {
              window.onunload = null;
              document.location.href = '/account/login/Logout.do';
          };

          window.showAlert = function (showMessage) {
              alert(showMessage);
              return;
          };
          // allow the Iframe to be resizable from PYW
          window.pywIframeVerticalResize = function (height) {
              var iFrame = document.getElementById('iframePage');
              iFrame.style.height = height + 'px';
              iFrame.height = height;
          }
      </script>
      <!-- <iframe id="iframePage" name="iframePage" class="h-920" src="https://uk.site.sports.betfair.com/reporting/bettingpandl/BettingPandL.do" frameborder="0"></iframe> -->
        <iframe id="iframePage" name="iframePage" class="h-920" src="{{route('form_account_statement')}}" frameborder="0"></iframe>
    </div>
  </div>
</div>

@endsection