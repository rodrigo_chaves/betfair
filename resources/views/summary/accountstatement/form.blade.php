<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
		<script type="text/javascript">
			//document.domain = "betfair.com";


			var exchangeCount = 1;

			var exchangeWalletDisplayNames = new Object();

			exchangeWalletDisplayNames[1] = "Carteira Principal:";


			function BetexProduct(id, type, subdomain, secureSubdomain) {
				this.id = id;
				var subdomain = subdomain;
				var secureSubdomain = secureSubdomain;
				var type = type;

				this.getDomain = function(secure) {
					return secure ? secureSubdomain : subdomain;
				}
				this.isExchange = function() {
					return type == "EXCH";
				}
			}

			var betexProducts = new Object();

			betexProducts[0] = new BetexProduct(0, "DOMAIN", "http://betfair.com", "https://betfair.com");


			betexProducts[1] = new BetexProduct(1, "EXCH", "https://uk.site.sports.betfair.com/", "https://uk.site.sports.betfair.com/");

			betexProducts[3] = new BetexProduct(3, "MULT", "https://site.multiples.betfair.com/", "https://site.multiples.betfair.com/");

			betexProducts[5] = new BetexProduct(5, "GAMEX", "https://site.games.betfair.com/", "https://site.games.betfair.com/");

			betexProducts[7] = new BetexProduct(7, "ORBIS", "https://site.arcade.betfair.com/", "https://site.arcade.betfair.com/");

			betexProducts[8] = new BetexProduct(8, "CASI", "https://site.casino.betfair.com/", "https://site.casino.betfair.com/");

			betexProducts[884] = new BetexProduct(884, "POKER", "https://site.poker.betfair.com/", "https://site.poker.betfair.com/");

			betexProducts[924] = new BetexProduct(924, "OPENB", "https://site.sportsbook.betfair.com/", "https://site.sportsbook.betfair.com/");

			betexProducts[1444] = new BetexProduct(1444, "POKER", "https://site.poker.betfair.com/", "https://site.poker.betfair.com/");


			function betex_getProductDomain(productId, secure, url) {
				if(!secure) { secure = false; }
				if(productId != 0 && !productId) { productId = 1; }
				if(!url) { url = ""; }
				if (betexProducts[productId]) {
					return betexProducts[productId].getDomain(secure) + url;
				} else {
					return "";
				}
			}

			function betex_getExchangeDomain(productId, secure, url) {
				return betex_getProductDomain(productId, secure, url);
			}

			function betex_iterateExchanges(callback) {
				for(var i in betexProducts) {
					if(betexProducts[i].isExchange()) {
						callback(betexProducts[i].id);
					}
				}
			}

		</script>

    <title>Betfair – bolsa de apostas online – apostas de desporto e entretenimento com probabilidades excelentes</title>
    <meta name="description" lang="en" content="A bolsa de apostas Betfair oferece probabilidades melhores e a possibilidade de apostar em eventos desportivos em directo. Aposte a favor ou contra em futebol, corridas de cavalos, críquete, ténis, râguebi, golfe e muitos outros desportos.">
    <meta name="keywords" lang="en" content="Betfair,bolsa de apostas online,probabilidades,futebol,corridas de cavalos,corridas de cães,golfe,críquete,apostas “Em Directo”,râguebi,ténis,probabilidades fixas,desportos motorizados,voleibol,atletismo,boxe,dardos,póquer,bacará,blackjack,texas hold’em,reality shows,big brother,pop idol">
    <meta name="robots" content="index,follow">
    <meta http-equiv="expires" content="-1">
    <meta http-equiv="content-type" charset="UTF-8">
    <link rel="shortcut icon" href="https://cache.ssl.site.sports.betfair.com/favicon.ico">
<link rel="stylesheet" href="https://cache.ssl.site.sports.betfair.com/exchange-web/resources/betfair/standard/common/content/css/Global-css.jsp" type="text/css">
<script language="Javascript" src="https://uk.site.sports.betfair.com/SpawningLibrary.do"></script>
<script language="Javascript" src="https://cache.ssl.site.sports.betfair.com/20.0.25.2/common/SpawningLibrary.js"></script>
<script language="Javascript" src="https://cache.ssl.site.sports.betfair.com/20.0.25.2/js/FormValidation.js"></script>
<script language="Javascript" src="https://cache.ssl.site.sports.betfair.com/20.0.25.2/user/Utilities.js"></script>
<script language="Javascript">
  function validate() {
      initialiseValidation("");

      if (validateFormEntryExists(document.forms[0].jumpTo,"Por favor introduza o número da página de destino.")) {
          if (validateFormEntryIsNumeric(document.forms[0].jumpTo,"Assegure-se de que introduz um número inteiro. ")) {
              validateFormEntryInRange(document.forms[0].jumpTo,"O número da página deve ser entre 1 e32.",1,32)
          }
      }

      if (finaliseValidation()) {
          jumpToPage();
      }
      else {
          return false;
      }
  }

  function decideProductDDDisplay(multiplesEnabled){
      
          var includeProductParameter = "-1";
      

      if (!(multiplesEnabled == "TRUE")){
          toggleProductDDVisibility(false);
      }else{
          if (includeProductParameter == "-1"){
              toggleProductDDVisibility(false);
          }else{
              toggleProductDDVisibility(true);
          }   
      }
  }

  function changeAccountSelect(){
      var sValue = document.forms[0].accountStatementSelector.options[document.forms[0].accountStatementSelector.selectedIndex].value;
      if (sValue == 'exge') {
        toggleDataRequestChargesHeadVisibility(false);
        var exchange = 1;
        if (exchange == 1 && isMultiplesEnabled() == "TRUE") {
          toggleProductDDVisibility(true);
          loadStatement(sValue,'all');
        } else {
          toggleProductDDVisibility(false);
          loadStatement(sValue,'exge');
        }
      } else if (sValue == 'drc') {
        toggleProductDDVisibility(false);
        toggleDataRequestChargesHeadVisibility(true);
        location.href = '/account/AccountStatement.do?dataRequestCharges=true';
      } else {
        toggleProductDDVisibility(false);
        toggleDataRequestChargesHeadVisibility(false);
        loadStatement(sValue,"-1");
      }
  }

  function changeDataRequestChargesTimeFrame(e1){
    location.href = '/account/AccountStatement.do?dataRequestCharges=true&timeFrame=' + e1.value;
  }

  function isMultiplesEnabled(){

      
          var isMultiplesEnabled = 'TRUE';
      

      return isMultiplesEnabled;
  }



  function changeProductDD(el) {

    var sValue = document.forms[0].accountStatementSelector.options[document.forms[0].accountStatementSelector.selectedIndex].value;
    var productDDvalue = el.value;

      if (productDDvalue == 'mult'){
          
          if (document.getElementById("AccountWrapper") != 'undefined'){
              document.getElementById("AccountWrapper").style.display = 'none';
          }
          
          if (document.getElementById("PaginationAccountWrapper") != 'undefined'){
              document.getElementById("PaginationAccountWrapper").style.display = 'none';
          }
          
          window.frames['multAccountManager'].document.location.href = 'https://site.multiples.betfair.com/multiples/account/MultipleAccountStatement.do';
          
          if (document.getElementById("MultipleAccountWrapper") != 'undefined'){
              document.getElementById("MultipleAccountWrapper").style.display = 'block';
          }
      }
      else{
          loadStatement(sValue,productDDvalue);
          
          if (document.getElementById("MultipleAccountWrapper") != 'undefined'){
              document.getElementById("MultipleAccountWrapper").style.display = 'none';
          }
          
          if (document.getElementById("AccountWrapper") != 'undefined'){
              document.getElementById("AccountWrapper").style.display = 'block';
          }
          
          if (document.getElementById("PaginationAccountWrapper") != 'undefined'){
              document.getElementById("PaginationAccountWrapper").style.display = 'block';
          }
      }
  }

  function loadStatement(sValue,pDDValue) {

      //var sURL = location.href = '/account/AccountStatement.do?startRecord=0&include='+sValue+'&includeProduct='+pDDValue;
      var sURL = "{{route('filter_account_statement')}}";
      sURL += "?startRecord=0&include="+sValue+"&includeProduct="+pDDValue;

      alert(sURL);
      location.href = sURL;
  }


  function displayForAustralia(){
    // Do not allow trading or agent masters to see the dropdown
    // Trading master viewing a subaccount and Standard accounts must display the dropdown

          if(parent.currentExchange == 2){
              document.getElementById("productDDtr").style.display = "none";
          }

    if(document.forms[0].accountStatementSelector.selectedIndex==0){
      try {
        document.getElementById("trShowMultiples").style.display = "block";
      }
      catch (e) {
      }
    }
  }

  function toggleProductDDVisibility(visible) {
      if (visible){
          document.getElementById("productDDtr").style.display = "block";
      }else{
          document.getElementById("productDDtr").style.display = "none";
      }
  }

  function toggleDataRequestChargesHeadVisibility(visible){
    var displayStr = "none";
    if(visible==true){
      displayStr = "block";
    }
    document.getElementById("dataRequestChargesHead").style.display = displayStr;
  }


  function jumpToPage() {
      var sIncludeType;
      if (document.forms[0].include){
          if (document.forms[0].include[0].checked) {
              sIncludeType=document.forms[0].include[0].value;
          }
          else {
              sIncludeType=document.forms[0].include[1].value;
          }
      }
      var sURL = '/account/AccountStatement.do?startRecord=' + ((document.forms[0].jumpTo.value-1)*20) + '&include=all&includeProduct=-1';

      location.href = sURL;
  }

  function adjustIFrameSize (iframeWindow) {
      if (iframeWindow.document.height) {
          var iframeElement = document.getElementById(iframeWindow.name);
              iframeElement.style.height = iframeWindow.document.height + 'px';
              iframeElement.style.width = iframeWindow.document.width + 'px';
      }else if (document.all) {
              var iframeElement = document.all[iframeWindow.name];
              
                  if (iframeWindow.document.compatMode && iframeWindow.document.compatMode != 'BackCompat') 
                  {
                      iframeElement.style.height = iframeWindow.document.documentElement.scrollHeight + 5 + 'px';
                      iframeElement.style.width = iframeWindow.document.documentElement.scrollWidth + 5 + 'px';
                  }else {                 
                      iframeElement.style.height = iframeWindow.document.body.scrollHeight + 5 + 'px';
                      iframeElement.style.width = iframeWindow.document.body.scrollWidth + 5 + 'px';
                  }
      }
  }

  function toggleOffsetPopup(show){
    var displayString = 'none';
    if(show == true || show == 'true'){
      displayString = 'block';
    }
    document.getElementById('offsetPopup').style.display = displayString;
  }
</script>
</head>


<body onload="parent.exchangeLoaded(1);displayForAustralia();decideProductDDDisplay(isMultiplesEnabled());" class="GlobalBackground" leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<form name="SearchResults" onsubmit="validate(); return false;">
<table border="0" cellspacing="1" cellpadding="0" width="100%">
    <tr>
        <td width="100%">
            <table border="0" cellspacing="0" cellpadding="10" width="100%">
                <tr>
                    <td class="GlobalHeadingBackground" width="100%">
                        <table border="0" cellspacing="0" cellpadding="0" width="100%">	
                            <tr>
                            	<td class="GlobalHeadingBackground" height="20"><span class="GlobalHeadingText">Extracto da conta : Carteira Principal</span></td>
                            </tr>

                            <tr>
                            	<td class="GlobalHeadingBackground" colspan="3" width="100%" height="20">
                            	<span class="GlobalHeadingText"><span class="HeaderText">
                            
                                     Augusto Cesar Gomes Queiroz
                                
                                 26-Jan-2018 22:23
                                    </span></span>
                                 </td>
                            </tr>
				    <tr>
					    <td class="GlobalHeadingBackground" height="18">
					    
			    				 
						    <span class="HeaderText">
							    Após a resolução de um Mercado o respectivo registo pode demorar até 10 minutos a ser adicionado ao seu Extracto de Conta.
						    </span>
			    			
					    </td>
				    </tr>
						    
							<tr id="trShowMultiples" style="display:none">
								<td class="GlobalHeadingBackground" height="18"><span class="HeaderText">Para aceder às transacções múltiplas seleccione ‘Apenas apostas de desporto’.</span></td>
							</tr>
							
                            <tr>
                                <td class="GlobalBackgroundBlueShade3">
                                	<table border="0" cellspacing="0" cellpadding="0">

					    
						
                       		<tr>
                       		<td nowrap height="24">
                            <select name="accountStatementSelector" onchange="changeAccountSelect()">
                                <option value="all" selected>Todas as Transacções</option>
                                <option value="exge" >Só Apostas Desportivas</option>
                                <option value="pr" >Só Sala de Póquer</option>
                                <option value="gamex" >Só Jogos Betfair</option>							 
                                <option value="arcade" >Só Jogos Arcade</option>
                                <option value="SKILLDICE" >Só Jogos Skill & Dados</option>
                                <option value="dpw" >Só depósitos, levantamentos e transferências</option>
                                <option value="drc" >Taxas de Pedidos de Dados </option>
                            </select>
                          </td>
                                      </tr>

                                      <tr id="productDDtr" style=" display:none"  >
										<td nowrap height="18">
											<table border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td><input type="radio" onclick="changeProductDD(this)" name="productDD" value="all" ></td>
													<td><span class="HeaderText">Resumo</span>&nbsp;&nbsp;</td>
													<td><input type="radio" onclick="changeProductDD(this)" name="productDD" value="exge" ></td>
													<td><span class="HeaderText">Apostas simples</span>&nbsp;&nbsp;</td>
													<td><input type="radio" onclick="changeProductDD(this)" name="productDD" value="mult" ></td>
													<td><span class="HeaderText">Apostas múltiplas</span>&nbsp;&nbsp;</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr id="dataRequestChargesHead" style="display:none;">
										<td nowrap height="18">
											<table border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td><input type="radio" onclick="changeDataRequestChargesTimeFrame(this)" name="drcGroup" value="0" checked="checked"/></td>
													<td><span class="HeaderText">Só para Hoje </span>&nbsp;&nbsp;</td>
													<td><input type="radio" onclick="changeDataRequestChargesTimeFrame(this)" name="drcGroup" value="1" /></td>
													<td><span class="HeaderText">Últimos 7 Dias </span>&nbsp;&nbsp;</td>
													<td><input type="radio" onclick="changeDataRequestChargesTimeFrame(this)" name="drcGroup" value="2" /></td>
													<td><span class="HeaderText">Últimos 30 Dias </span>&nbsp;&nbsp;</td>
													<td><input type="radio" onclick="changeDataRequestChargesTimeFrame(this)" name="drcGroup" value="3" /></td>
													<td><span class="HeaderText">Últimos 3 Meses </span>&nbsp;&nbsp;</td>
												</tr>
												<tr>
													<td colspan="8">
														&nbsp;&nbsp;<a href="javascript:spawn('betfairCharges');">Mais informação sobre as Taxas de Pedidos de Dados </a>
													</td>
												</tr>
											</table>
										</td>
									</tr>
                                </table>
                                </td>
                            </tr>
                       </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>



    <tr>
        <td width="100%">
            <div id="AccountWrapper" style="display:block;">
            <table border="0" cellspacing="0" cellpadding="2" width="100%">            
                <tr align="center">
                    <td class="GlobalTableHeaderBackground"><img src="https://cache.ssl.site.sports.betfair.com/exchange-web/resources/betfair/standard/common/content/images/common/spacer.gif" height="1" width="1" border="0"></td>
                    <td class="GlobalTableHeaderBackground" align="left"><span class="GlobalBoldedText">Fixada</span></td>
                    <td class="GlobalTableHeaderBackground"><span class="GlobalBoldedText">Ref Id</span></td>
                    <td class="GlobalTableHeaderBackground" align="left"><span class="GlobalBoldedText">Colocada</span></td>
                    <td class="GlobalTableHeaderBackground" align="left"><span class="GlobalBoldedText">Descrição</span></td>
                    <td class="GlobalTableHeaderBackground"><span class="GlobalBoldedText">&nbsp;</span></td>
                    
	 	    
				
					<td class="GlobalTableHeaderBackground"><span class="GlobalBoldedText">Média probabilidades</span></td>
					<td class="GlobalTableHeaderBackground"><span class="GlobalBoldedText">Parada</span></td>
					<td class="GlobalTableHeaderBackground"><span class="GlobalBoldedText">Estado</span></td>
				
		  	
				    
                    <td class="GlobalTableHeaderBackground"><span class="GlobalBoldedText">RD<br>(€)</span></td>
                    <td class="GlobalTableHeaderBackground"><span class="GlobalBoldedText">RC<br>(€)</span></td>
                    <td class="GlobalTableHeaderBackground"><span class="GlobalBoldedText">Saldo<br>(€)</span></td>
                    <td class="GlobalTableHeaderBackground"><img src="https://cache.ssl.site.sports.betfair.com/exchange-web/resources/betfair/standard/common/content/images/common/spacer.gif" height="1" width="1" border="0"></td>
                </tr>

                @foreach($account_statements as $statement)
                  <tr align="center">
                    <td class="GlobalTableCell">
                      <img src="https://cache.ssl.site.sports.betfair.com/exchange-web/resources/betfair/standard/common/content/images/common/spacer.gif" height="1" width="1" border="0">
                    </td>
                    <td class="GlobalTableCell" align="left" width="80">{{$statement->createdFormated()}}</td>
                    <td class="GlobalTableCell">{{$statement->ref_id}}</td>
                    <td class="GlobalTableCell" align="left" width="80">{{$statement->placedFormated()}}</td>
                    <td class="GlobalTableCell" align="left">
                      <span class="GlobalBoldedText">{{$statement->description}}</span>
                    </td>
                    <td class="GlobalTableCell">&nbsp;</td>
                    <td class="GlobalTableCell">{{number_format($statement->average_odds,2,',','.')}}</td>
                    <td class="GlobalTableCell">{{number_format($statement->stoped,2,',','.')}}</td>
                    <td class="GlobalTableCell">{{$statement->state}}</td>
                    <td class="GlobalTableCell">{{number_format($statement->rd,2,',','.')}}</td>
                    <td class="GlobalTableCell">{{number_format($statement->rc,2,',','.')}}</td>
                    <td class="GlobalTableCell">{{number_format($statement->balance,2,',','.')}}</td>
                  </tr>
                @endforeach
            </table>
            </div>
        </td>
    </tr>


    <tr>
        <td width="100%">
            <div id="PaginationAccountWrapper" style="display:block;">
            <table border="0" cellspacing="0" cellpadding="3" width="100%">
                <tr>
                    <td class="GlobalBackground"><img src="https://cache.ssl.site.sports.betfair.com/exchange-web/resources/betfair/standard/common/content/images/common/spacer.gif" height="1" width="1" border="0"></td>
                    <td class="GlobalBackground" width="100%">
                      {{$account_statements->links()}}
                    </td>
                    <td class="GlobalBackground"><img src="https://cache.ssl.site.sports.betfair.com/exchange-web/resources/betfair/standard/common/content/images/common/spacer.gif" height="1" width="1" border="0"></td>
                </tr>
                <tr>
                    <td class="GlobalBackground"><img src="https://cache.ssl.site.sports.betfair.com/exchange-web/resources/betfair/standard/common/content/images/common/spacer.gif" height="1" width="1" border="0"></td>
                    <td class="GlobalBackground">
                        <table cellspacing="0" cellpadding="0" border="0">
                                <td class="GlobalBackground"><span class="GlobalGreyText">Todas as horas são</span>&nbsp;</td>
                                <td class="GlobalBackground"><span class="GlobalGreyText">BRT&nbsp;</span></td>
                                
                                
						            <td class="GlobalBackground"><img src="https://cache.ssl.site.sports.betfair.com/exchange-web/resources/betfair/standard/common/content/images/common/spacer.gif" width="1" height="1" border="0"><br><a href="javascript:if (parent) { parent.getHelp('Help.Managing.Account','TimeZones'); }" class="GlobalHelpLink">&nbsp;?&nbsp;</a>&nbsp;<br><img src="https://cache.ssl.site.sports.betfair.com/exchange-web/resources/betfair/standard/common/content/images/common/spacer.gif" width="1" height="1" border="0"></td>
                                    <td class="GlobalBackground"><span class="GlobalGreyText">excepto indicação em contrário.</span></td>
			    			    
                            </tr>
                        </table>
                    </td>
                    <td class="GlobalBackground"><img src="https://cache.ssl.site.sports.betfair.com/exchange-web/resources/betfair/standard/common/content/images/common/spacer.gif" height="1" width="1" border="0"></td>
                </tr>
            </table>
            </div>
        </td>
    </tr>


    <tr>
        <td width="100%">
            <div id="MultipleAccountWrapper" style="OVERFLOW:visible; display:none; width:100%;HEIGHT:250px; border-width:thin;border-style:none;">
                <table border="0" cellspacing="0" cellpadding="2" width="100%">
                    <tr>
                        <td width="100%">
                            <iframe name="multAccountManager" id="multAccountManager" frameborder="0" scrolling="no" height="800px" width="100%" src="https://uk.site.sports.betfair.com/blank.jsp"></iframe>
                        </td>
                    </tr>   

                    <tr>
                        <td class="GlobalBackground">
                            <table cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td class="GlobalBackground"><span class="GlobalGreyText">&nbsp;&nbsp;Todas as horas são</span>&nbsp;</td>
                                    <td class="GlobalBackground"><span class="GlobalGreyText">BRT&nbsp;</span></td>
                                    <td class="GlobalBackground"><img src="https://cache.ssl.site.sports.betfair.com/exchange-web/resources/betfair/standard/common/content/images/common/spacer.gif" width="1" height="1" border="0"><br><a href="javascript:if (parent) { parent.getHelp('Help.Managing.Account','TimeZones'); }" class="GlobalHelpLink">&nbsp;?&nbsp;</a>&nbsp;<br><img src="https://cache.ssl.site.sports.betfair.com/exchange-web/resources/betfair/standard/common/content/images/common/spacer.gif" width="1" height="1" border="0"></td>
                                    <td class="GlobalBackground"><span class="GlobalGreyText">excepto indicação em contrário.</span></td>
                                </tr>
                            </table>
                        </td>
                    </tr>   

                </table>
            </div>
        </td>
    </tr>

	
		
	

    <tr>
	    <td width="100%">
	        <div id="AccountWrapper" style="display:block;">
	        <table border="0" cellspacing="0" cellpadding="10" width="100%">
	            <tr>
	                <td class="GlobalBackground" width="100%">
	                	<a href="/reporting/ViewTransferHistoryAction.do">Transferências internas</a>
	                </td>
	            </tr>
	        </table>
	        </div>  
	    </td>
	</tr>



		
	


    
</table>
</form>
</body>
</html>
