<!DOCTYPE html>
<head>
    <title>Betfair | My Account</title>
    <link rel="shortcut icon" href="/page/betfair/images/icon/favicon.ico">
    <link rel="apple-touch-icon-precomposed" href="/page/betfair/image/icon/apple-touch-icon.png">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" >
    <script type='text/javascript' src='//service.maxymiser.net/api/eu/new_betfair.com/7bb08d/mmapi.js'> </script>
    <link href="https://ie2-sscbf.cdnppb.net/static/all-14bc1113faca3fbae90d837170b746bf.css" type="text/css" rel="stylesheet">
    <link href="https://ie1-mawbf.cdnppb.net//resources/bundles/yui_dialog_dialog-xchannel_fancy-select-box-betfair_module-info_platformapi_moduleapi_module_feedback-uservoice_footer-new-betfair_footer-widgets-betfair_form-fields_kyc-banner-betfair_module-default-core_platformapi-xchannel_moduleapi-xchannel_module-xchannel_mybets-min_navigator-betfair_news-banner-betfair_returntosite-betfair_summary-balance-betfair_summary-usage-reporting/all_413_.css" type="text/css" rel="stylesheet">
    <link href="https://ie2-mawbf.cdnppb.net//resources/bundles/yui_dialog_dialog-xchannel_fancy-select-box_module-info_platformapi_moduleapi_module_feedback-uservoice_footer-new-betfair_footer-widgets-betfair_iframe_module-default-core_platformapi-xchannel_moduleapi-xchannel_module-xchannel_navigator-betfair_news-banner-betfair_returntosite-betfair/all_413_.css" type="text/css" rel="stylesheet">
    <script type="text/javascript">
        function scrollIframeTop() {
            if (window.self !== window.top) {
                setTimeout(function () {
                    window.parent.scrollTo(0, 0);
                }, 0);
            }
        }
        if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
            window.onload = scrollIframeTop;
        }
    </script>
</head>
<body class="layout-desktop_template pt jsDisabled">
    <div id="grid-container" class="concept flexible-grid ">
        @include('layout.elements.header')
        @include('layout.elements.main_navigation')

        <div id="content-container" class="main-content-container main-content-desktop-container__page">
          <div class="content main-content-container__page">
            @yield('content')
          </div>
        </div>

        @include('layout.elements.footer')
    </div>
  </body>
</html>