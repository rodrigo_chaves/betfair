<div id="main-navigation" role="banner">
        <div class="nav-wrapper clearfix">
            <div id="mod-returntosite-77257-container" class="mod">
                <div id="mod-returntosite-77257" class="mod-returntosite mod-returntosite-returntosite">
                    <div class="returnToSite-container returnToSite-container-new">
                        <span class="back-button" id="backButton">Regressar às apostas</span>
                    </div>
                </div>
            </div>
            <div id="mod-navigator-77256-container" class="mod">
                <div id="mod-navigator-77256" class="mod-navigator mod-navigator-navigator">
                    <ul class="i6" data-qa="navigation-tabs">
                        <li class="link-group home selected">
                            <span class="selected single">
                            RESUMO
                            </span>
                        </li>
                        <li class="link-group">
                            <span class="label">
                            Pagamentos
                            </span>
                            <ul class="dropdown" data-group-tag="Payments">
                                <li class="sub-link">
                                    <a href="https://myaccount.betfair.com/payments/deposit" data-page-tag="Deposit">
                                    Depósito
                                    </a>
                                </li>
                                <li class="sub-link">
                                    <a href="https://myaccount.betfair.com/payments/withdraw" data-page-tag="Withdraw">
                                    Levantamento
                                    </a>
                                </li>
                                <li class="sub-link">
                                    <a href="https://myaccount.betfair.com/payments/transfer" data-page-tag="Transfer">
                                    Transferir
                                    </a>
                                </li>
                                <li class="sub-link">
                                    <a href="https://myaccount.betfair.com/payments/carddetails" data-page-tag="Card Details">
                                    Detalhes do cartão
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="link-group">
                            <span class="label">
                            Registo de atividades
                            </span>
                            <ul class="dropdown" data-group-tag="Betting Activity">
                                <li class="sub-link">
                                    <a href="https://myactivity.betfair.com/#/sportsbook" data-page-tag="Sportsbook Bets">
                                    Apostas no Sportsbook
                                    </a>
                                </li>
                                <li class="sub-link">
                                    <a href="https://myactivity.betfair.com/#/exchange" data-page-tag="Exchange Bets">
                                    Apostas no Intercâmbio
                                    </a>
                                </li>
                                <li class="sub-link">
                                    <a href="https://myaccount.betfair.com/activity/currentbets" data-page-tag="Exchange Current Bets">
                                    Apostas atuais no Intercâmbio
                                    </a>
                                </li>
                                <li class="sub-link">
                                    <a href="https://myaccount.betfair.com/activity/bettinghistory" data-page-tag="Exchange Betting History">
                                    Histórico de Apostas no Intercâmbio
                                    </a>
                                </li>
                                <li class="sub-link">
                                    <a href="{{route('profitandloss')}}" data-page-tag="Exchange Profit &amp; Loss">
                                    Ganhos e perdas no Intercâmbio
                                    </a>
                                </li>
                                <li class="sub-link">
                                    <a href="{{route('account_statement')}}" data-page-tag="Account Statement">
                                    Extrato de conta
                                    </a>
                                </li>
                                <li class="sub-link">
                                    <a href="https://myactivity.betfair.com/#/transactions" data-page-tag="Transaction History">
                                    Histórico de Transacções
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="link-group">
                            <span class="label">
                            Os meus detalhes
                            </span>
                            <ul class="dropdown" data-group-tag="Account Details">
                                <li class="sub-link">
                                    <a href="https://myaccount.betfair.com/accountdetails/mydetails" data-page-tag="My Details">
                                    Detalhes da conta
                                    </a>
                                </li>
                                <li class="sub-link">
                                    <a href="https://myaccount.betfair.com/accountdetails/mysecurity?showAPI=1" data-page-tag="My Security">
                                    Definições de segurança
                                    </a>
                                </li>
                                <li class="sub-link">
                                    <a href="https://myaccount.betfair.com/accountdetails/document-uploader" data-page-tag="Document Uploader">
                                    Upload de Documentos
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="link-group">
                            <span class="label">
                            Promoções e recompensas
                            </span>
                            <ul class="dropdown" data-group-tag="Promotions &amp; Rewards">
                                <li class="sub-link">
                                    <a href="https://promos.betfair.com/sport" data-page-tag="Promotions">
                                    Promoções
                                    </a>
                                </li>
                                <li class="sub-link">
                                    <a href="https://promos.betfair.com/mybonuses" data-page-tag="My Bonuses">
                                    Meus Bônus
                                    </a>
                                </li>
                                <li class="sub-link">
                                    <a href="https://myaccount.betfair.com/rewards/points-and-holidays" data-page-tag="Points &amp; Holidays">
                                    Pontos e férias
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="link-group">
                            <a class="single" href="https://myaccount.betfair.com/playerprotection" data-page-tag="Player Protection">
                            Proteção do jogador
                            </a>
                        </li>
                    </ul>
                    <script type="text/javascript" src="https://site.sports.betfair.com/SpawningLibrary.do"></script>
                    <script type="text/javascript" src="https://accountservices.betfair.com/domains.js.jsp"></script>
                </div>
            </div>
        </div>
    </div>