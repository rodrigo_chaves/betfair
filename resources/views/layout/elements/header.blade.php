<div id="header-container" role="banner">
        <div id="mod-header-77258-container" class="mod">
            <div id="mod-header-77258" class="mod-header mod-header-header">
                <div class="header">
                    <div id="ssc-header-container" class="ssc-container ssc-theme-betfair ssc-no-js ssc-loggedIn  ssc-prod-my-account ssc-jur-intl ssc-reg-all_regions ssc-lang-pt ssc-cc-br ssc-cha-desktop">
                        <div id="header">
                            <div id="mod-header-594584-container" class="mod">
                                <div id="mod-header-594584" class="mod-header mod-header-header">
                                    <div id="ssc-hw" class="isLoggedIn">
                                        <div class="ssc-hc">
                                            <table id="ssc-ht" cellspacing="0" cellpadding="0">
                                                <tbody><tr>
                                                    <td><a class="ssc-lg ssc-lg-my_account" href="http://www.betfair.com/" target="_top" title="Página inicial da Betfair" data-gtml="betfair logo"></a></td>
                                                    <td>
                                                        <div class="ssc-ili">
                                                            <div class="ssc-bi">
                                                            </div>
                                                            <div class="ssc-wmc ssc-hdn">
                                                                <div id="mod-web-messaging-594592-container" class="mod">
                                                                    <div id="mod-web-messaging-594592" class="mod-web-messaging mod-web-messaging-web-messaging">
                                                                        <a class="ssc-wm has-tooltip">
                                                                            <span class="ssc-wmi"></span><span class="ssc-mc">
                                                                            </span>
                                                                            <div class="ssc-wmd ssc-hdn">
                                                                                <div class="ssc-tlp">
                                                                                    <div class="ssc-tlpa"></div>
                                                                                    <div class="ssc-tlpc">
                                                                                        <ul></ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td id="ssc-ssec">
                                                    </td>
                                                    <td class="ssc-ilo">
                                                        <div id="ssc-lsec">
                                                            <div id="mod-login-594593-container" class="mod">
                                                                <div id="mod-login-594593" class="mod-login mod-login-login">
                                                                    <form class="ssc-lof" action="https://identitysso.betfair.com/api/logout" method="POST" autocomplete="off" novalidate="">
                                                                        <input id="ssc-lop" value="my-account" name="product" type="hidden">
                                                                        <input id="ssc-lorm" value="POST" name="redirectMethod" type="hidden">
                                                                        <input id="ssc-loru" value="https://myaccount.betfair.com/logout/success?rurl=http%3A%2F%2Fwww.betfair.com%2Fsport" name="url" type="hidden">
                                                                        <div class="ssc-ud ssc-udrl">
                                                                            <a class="ssc-unc" tabindex="0">
                                                                            <span class="ssc-un">A minha conta</span>
                                                                            <span class="ssc-uni"></span>
                                                                            </a>
                                                                            <span class="ssc-aru ssc-arus" tabindex="0">
                                                                            </span>
                                                                            <div class="ssc-md ssc-hdn-js">
                                                                                <div class="ssc-tlp">
                                                                                    <div class="ssc-tlpc ssc-tlpcs">
                                                                                        <div class="ssc-myal">
                                                                                            <a class="ssc-myau" data-hj-masked="">augustoqueiroz</a>
                                                                                            <a href="https://myaccount.betfair.com/summary/accountsummary" class="ssc-myBetfairAccount " target="_top" title="A minha conta" data-gtml="myBetfairAccount">A minha conta</a>
                                                                                            <a href="https://casino.betfair.com/cashier" class="ssc-myCasinoAccount " target="_top" title="" data-gtml="myCasinoAccount">A minha conta do Casino</a>
                                                                                            <a href="https://poker.betfair.com/web-cashier" class="ssc-myPokerAccount " target="_blank" title="" data-gtml="myPokerAccount">A minha conta de Poker</a>
                                                                                            <a href="http://site.games.betfair.com/account/MyAccount.do?tabID=1" class="ssc-myExchangeGamesAccount  sscmw width(1050) height(910) autoscroll(true)" target="_popup" title="" data-gtml="myExchangeGamesAccount">A minha conta de Jogos Exchange</a>
                                                                                            <a class="ssc-myalo" data-gtmdt="header my account dropdown" data-gtml="logout"><input id="ssc-lis" value="Sair" type="submit"></a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <a href="https://myaccount.betfair.com/account/home?rlhm=0&amp;tabID=3&amp;noLHM=1" target="_top" title="" data-gtml="Deposit" class="ssc-cta ssc-cta-primary ssc-depb">
                                                                        <span>Depósito</span>
                                                                        </a>
                                                                        <div class="ssc-hld">&nbsp;</div>
                                                                        <!--
                                                                            -->
                                                                        <div class="ssc-wl ssc-inv-js ssc-wlco">
                                                                            <div class="ssc-wlc" tabindex="0">
                                                                                <a class="ssc-sb ssc-hdn" data-gtml="show balance">Mostrar saldo</a>
                                                                                <table class="ssc-wldw" cellspacing="0" cellpadding="0">
                                                                                    <tbody><tr class="ssc-twlc " rel="main-wallet">
                                                                                        <td class="ssc-wln">
                                                                                            Principal
                                                                                        </td>
                                                                                        <td class="ssc-wla" rel="main">{{number_format($user->balance->master,2)}}</td>
                                                                                    </tr>
                                                                                </tbody></table>
                                                                            </div>
                                                                            <span class="ssc-aru ssc-arus" tabindex="0">
                                                                            </span>
                                                                            <div class="ssc-bd ssc-hdn-js">
                                                                                <div class="ssc-tlp">
                                                                                    <div class="ssc-tlpc ssc-tlpcs">
                                                                                        <table class="ssc-wldd" cellspacing="0" cellpadding="0">
                                                                                            <tbody><tr class="ssc-wlt ssc-wll ssc-wlrmw">
                                                                                                <td class="ssc-wla" rel="total">{{number_format(($user->balance->master + $user->balance->exchange),2)}}</td>
                                                                                                <td class="ssc-wlnc">
                                                                                                    <div class="ssc-wln">Saldos em dinheiro</div>
                                                                                                </td>
                                                                                                <td class="ssc-wlli">
                                                                                                    <a class="ssc-hb ssc-wlcp" data-gtmdt="header dropdown" data-gtml="hide balance">Ocultar saldo</a>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr class="ssc-wlrd">
                                                                                                <td colspan="3"></td>
                                                                                            </tr>
                                                                                            <tr class="ssc-wlm ssc-wlrmw">
                                                                                                <td class="ssc-wla" rel="main">{{number_format($user->balance->master,2)}}</td>
                                                                                                <td class="ssc-wlnc">
                                                                                                    <div class="ssc-wln">Carteira principal</div>
                                                                                                </td>
                                                                                                <td class="ssc-wlli">
                                                                                                    <a class="ssc-wlcp ssc-depo " href="https://myaccount.betfair.com/account/home?rlhm=0&amp;tabID=3" target="_top" title="" data-gtmdt="header dropdown" data-gtml="deposit main wallet">Depósito</a>
                                                                                                    <span class="ssc-wlld">|</span>
                                                                                                    <a class="ssc-wlcp ssc-depo " href="https://myaccount.betfair.com/account/home?rlhm=0&amp;tabID=4" target="_top" title="" data-gtmdt="header dropdown" data-gtml="withdraw main wallet">Levantamento</a>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr><td>&nbsp;</td>
                                                                                            <td class="ssc-wlnc">
                                                                                                <div class="ssc-wld">
                                                                                                    (Sportsbook, Exchange, Arcade, Casino, Bingo)
                                                                                                </div>
                                                                                            </td>
                                                                                            <td>&nbsp;</td>
                                                                                            </tr>
                                                                                            <tr class="ssc-wlddr ssc-wlrmw">
                                                                                                <td class="ssc-wla" rel="xg">{{number_format($user->balance->exchange,2)}}</td>
                                                                                                <td class="ssc-wlnc">
                                                                                                    <div class="ssc-wln">Carteira dos Jogos Exchange</div>
                                                                                                </td>
                                                                                                <td class="ssc-wlli ssc-wltl">
                                                                                                    <a class="ssc-wlcp ssc-depo " href="https://myaccount.betfair.com/payments/transfer" target="_top" title="" data-gtmdt="header dropdown" data-gtml="transfer exchange games wallet">Transferir</a>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr class="ssc-wlddr ssc-wlrmw">
                                                                                                <td class="ssc-wla" rel="poker">A carregar...</td>
                                                                                                <td class="ssc-wlnc">
                                                                                                    <div class="ssc-wln">Carteira da Betfair Poker</div>
                                                                                                </td>
                                                                                                <td class="ssc-wlli ssc-wltl">
                                                                                                    <a class="ssc-wlcp ssc-depo " href="https://myaccount.betfair.com/payments/transfer" target="_top" title="" data-gtmdt="header dropdown" data-gtml="transfer poker wallet">Transferir</a>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr class="ssc-wlrd2">
                                                                                                <td colspan="3"></td>
                                                                                            </tr>
                                                                                            <tr class="ssc-wlpb">
                                                                                                <td colspan="3" class="ssc-wll">
                                                                                                    <div class="ssc-wln">
                                                                                                        Saldos de bónus
                                                                                                    </div>
                                                                                                    <div class="ssc-wld">(indisponível para levantamento)</div>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr class="ssc-wlrpw sportsbook-section">
                                                                                                <td class="ssc-wlli ssc-wla" rel="sportsbook_bonus">A carregar...</td>
                                                                                                <td class="ssc-wlnc">
                                                                                                    <div class="ssc-wln">
                                                                                                        Sportsbook Bônus – Total
                                                                                                    </div>
                                                                                                </td>
                                                                                                <td class="ssc-wlli ssc-wlcMyBonuses">
                                                                                                    <a href="https://promos.betfair.com/mybonuses/sport" target="_top">
                                                                                                    Ver detalhes
                                                                                                    </a>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr class="ssc-ddbd  ssc-hdn">
                                                                                                <td class="ssc-wla" rel="sportsbook_bonus_wagering">A carregar...</td>
                                                                                                <td>Bônus com requisitos de aposta</td>
                                                                                            </tr>
                                                                                            <tr class="ssc-ddbd  ssc-hdn">
                                                                                                <td class="ssc-wla" rel="frozen">A carregar...</td>
                                                                                                <td>Ganhos pendentes - Apostas em aberto</td>
                                                                                            </tr>
                                                                                            <tr class="ssc-ddbd  ssc-hdn">
                                                                                                <td class="ssc-wla" rel="sportsbook_bonus_cash">A carregar...</td>
                                                                                                <td>Bônus em apostas grátis</td>
                                                                                            </tr>
                                                                                            <tr class="ssc-wlrpw">
                                                                                                <td class="ssc-wlli ssc-wla ssc-hdn" rel="casino_bonus">A carregar...</td>
                                                                                                <td class="ssc-wlli ssc-wlcgb ssc-wlcgcb">
                                                                                                    <a class="ssc-wlrcb" data-gtmdt="header dropdown" data-gtml="get casino balance">Obter saldo</a>
                                                                                                </td>
                                                                                                <td class="ssc-wlnc">
                                                                                                    <div class="ssc-wln">Bónus do Casino</div>
                                                                                                </td>
                                                                                                <td class="ssc-wlli">&nbsp;</td>
                                                                                            </tr>
                                                                                            <tr class="ssc-wlrpw">
                                                                                                <td class="ssc-wlli ssc-wla" rel="arcade_bonus">A carregar...</td>
                                                                                                <td class="ssc-wlnc">
                                                                                                    <div class="ssc-wln">Bónus de Arcada</div>
                                                                                                </td>
                                                                                                <td class="ssc-wlli">&nbsp;</td>
                                                                                            </tr>
                                                                                            <tr class="ssc-wlrpw">
                                                                                                <td class="ssc-wlli ssc-wla" rel="xg_bonus">A carregar...</td>
                                                                                                <td class="ssc-wlnc">
                                                                                                    <div class="ssc-wln">Bónus dos Jogos Exchange</div>
                                                                                                </td>
                                                                                                <td class="ssc-wlli">&nbsp;</td>
                                                                                            </tr>
                                                                                            <tr class="ssc-wlrd2">
                                                                                                <td colspan="3"></td>
                                                                                            </tr>
                                                                                            <tr class="ssc-wlas">
                                                                                                <td colspan="3">Vê os detalhes completos em <a class="ssc-wlcp ssc-depo " href="https://myaccount.betfair.com/summary/accountsummary" target="_top" title="" data-gtmdt="header dropdown" data-gtml="account summary">Extrato de conta</a></td>
                                                                                            </tr>
                                                                                        </tbody></table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <a class="ssc-rl ssc-hdn-nojs" tabindex="0" title="Atualizar o saldo"><span>&nbsp;</span></a>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    </td><td>
                                                        <div class="ssc-hlsw">
                                                            <div class="ssc-hls" tabindex="0"><span class="ssc-lsf ssc-fpt"></span><span class="ssc-aru"></span></div>
                                                            

                                                            <!-- CORRIGIR 


                                                            <div class="ssc-tlp ssc-hlsd ssc-hdn-js">
                                                                <div class="ssc-tlpc">
                                                                    <ul>
                                                                        <li>
                                                                            <a href="/en/GBR/summary/accountsummary"
                                                                                class="ssc-GBR"
                                                                                data-region="GBR"
                                                                                data-language="en"
                                                                                target="_self"><span class="ssc-ldf ssc-fgbr"></span>English - UK</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="/en/IRL/summary/accountsummary"
                                                                                class="ssc-IRL"
                                                                                data-region="IRL"
                                                                                data-language="en"
                                                                                target="_self"><span class="ssc-ldf ssc-firl"></span>English - IRE</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="/da/summary/accountsummary"
                                                                                class="ssc-da"
                                                                                data-region="GBR"
                                                                                data-language="da"
                                                                                target="_self"><span class="ssc-ldf ssc-fda"></span>Dansk</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="/de/summary/accountsummary"
                                                                                class="ssc-de"
                                                                                data-region="GBR"
                                                                                data-language="de"
                                                                                target="_self"><span class="ssc-ldf ssc-fde"></span>Deutsch</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="/el/summary/accountsummary"
                                                                                class="ssc-el"
                                                                                data-region="GBR"
                                                                                data-language="el"
                                                                                target="_self"><span class="ssc-ldf ssc-fel"></span>Ελληνικά</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="/es/summary/accountsummary"
                                                                                class="ssc-es"
                                                                                data-region="GBR"
                                                                                data-language="es"
                                                                                target="_self"><span class="ssc-ldf ssc-fes"></span>Español</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="/it/summary/accountsummary"
                                                                                class="ssc-it"
                                                                                data-region="GBR"
                                                                                data-language="it"
                                                                                target="_self"><span class="ssc-ldf ssc-fit"></span>Italiano</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="/pt/summary/accountsummary"
                                                                                class="ssc-pt"
                                                                                data-region="GBR"
                                                                                data-language="pt"
                                                                                target="_self"><span class="ssc-ldf ssc-fpt"></span>Português</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="/ru/summary/accountsummary"
                                                                                class="ssc-ru"
                                                                                data-region="GBR"
                                                                                data-language="ru"
                                                                                target="_self"><span class="ssc-ldf ssc-fru"></span>Русский</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="/sv/summary/accountsummary"
                                                                                class="ssc-sv"
                                                                                data-region="GBR"
                                                                                data-language="sv"
                                                                                target="_self"><span class="ssc-ldf ssc-fsv"></span>Svenska</a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>

                                                            -->

                                                        </div>
                                                    </td>
                                                    
                                                </tr>
                                            </tbody></table>
                                            <div id="ssc-hbt">
                                                <div id="mod-main-navigation-594594-container" class="mod">
                                                    <div id="mod-main-navigation-594594" class="mod-main-navigation mod-main-navigation-main-navigation">
                                                        <div class="ssc-sac ssc-sacn">
                                                            <a href="http://content.betfair.com/misc/?product=portal&amp;sWhichKey=ResponsibleGambling&amp;region=GBR&amp;locale=pt&amp;brand=betfair" target="_blank" title="Jogo responsável" data-gtml="Responsible Gambling">Jogo responsável</a>
                                                        </div>
                                                        <ul id="ssc-pn" class="ssc-np">
                                                            <li>
                                                                <a id="EXCHANGE" class=" " href="http://www.betfair.com/exchange" target="_top" title="Intercâmbio" data-gtml="1-EXCHANGE">Intercâmbio</a>
                                                            </li>
                                                            <li>
                                                                <a id="SPORTSBOOK" class=" " href="https://www.betfair.com/sport/home" target="_top" title="Sportsbook" data-gtml="2-SPORTSBOOK">Sportsbook</a>
                                                            </li>
                                                            <li>
                                                                <div class="ssc-tuc">
                                                                    <div class="ssc-tu">
                                                                        <p class="ssc-tui">NOVO</p>
                                                                    </div>
                                                                    <div class="ssc-tua"></div>
                                                                </div>
                                                                <a id="POOLS" class=" " href="https://pools.betfair.com" target="_top" title="LotoEsporte" data-gtml="3-POOLS">LotoEsporte</a>
                                                            </li>
                                                            <li>
                                                                <a id="CASINO" class=" " href="https://casino.betfair.com/" target="_top" title="Casino" data-gtml="4-CASINO">Casino</a>
                                                            </li>
                                                            <li>
                                                                <a id="LIVE_CASINO" class=" " href="http://casino.betfair.com/live" target="_top" title="Live Casino" data-gtml="5-LIVE_CASINO">Casino Live</a>
                                                            </li>
                                                            <li>
                                                                <a id="POKER" class=" " href="https://poker.betfair.com" target="_top" title="Poker" data-gtml="6-POKER">Poker</a>
                                                            </li>
                                                            <li>
                                                                <a id="EXCHANGE_GAMES" class=" " href="http://games.betfair.com/" target="_top" title="Exchange Games" data-gtml="7-EXCHANGE_GAMES">Jogos Exchange</a>
                                                            </li>
                                                            <li>
                                                                <a id="ARCADE" class=" " href="http://arcade.betfair.com/" target="_top" title="Arcade" data-gtml="8-ARCADE">Arcade</a>
                                                            </li>
                                                            <li>
                                                                <a id="VEGAS_SLOTS" class=" " href="https://vegasslots.betfair.com/" target="_top" title="Vegas Slots" data-gtml="9-VEGAS_SLOTS">Vegas Slots</a>
                                                            </li>
                                                            <li>
                                                                <a id="MACAU" class=" " href="https://macau.betfair.com" target="_top" title="Macau" data-gtml="10-MACAU">Macau</a>
                                                            </li>
                                                            <li>
                                                                <a id="BINGO" class=" " href="https://bingo.betfair.com/" target="_top" title="Bingo" data-gtml="11-BINGO">Bingo</a>
                                                            </li>
                                                            <li>
                                                                <a id="VIRTUAL_SPORTS" class=" " href="https://www.betfair.com/sport/virtuals" target="_top" title="Desportos Virtuais" data-gtml="12-VIRTUAL_SPORTS">Desportos Virtuais</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>