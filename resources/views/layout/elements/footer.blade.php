<div id="footer-container" role="contentinfo">
    <div id="mod-footer_new-77250-container" class="mod">
        <div id="mod-footer_new-77250" class="mod-footer_new mod-footer_new-footer_new">
            <div class="footer-new">
                <div id="ssc-footer-container" class="ssc-container ssc-theme-betfair ssc-no-js ssc-loggedIn  ssc-prod-my-account ssc-jur-intl ssc-reg-all_regions ssc-lang-pt ssc-cc-br ssc-cha-desktop">
                    <div id="footer">
                        <div id="mod-footer-594589-container" class="mod">
                            <div id="mod-footer-594589" class="mod-footer mod-footer-footer">
                                <div class="ssc-fw isLoggedIn ssc-fw-sm">
                                    <div class="ssc-flkh">
                                        <ul class="ssc-flnk">
                                            <li class="ssc-lw  "><a href="http://www.betfair.com/aboutUs/Privacy.Policy/" class="sscmw width(1000) height(600) autoscroll(true) ssc-privacyPolicy" target="_popup" title="Política de Privacidade" data-gtml="privacyPolicy">Política de Privacidade</a></li>
                                            <li class="ssc-lw  "><a href="http://www.betfair.com/aboutUs/Cookie.Policy/" class="sscmw width(1000) height(600) autoscroll(true) ssc-cookiePolicy" target="_popup" title="Política de cookies" data-gtml="cookiePolicy">Política de cookies</a></li>
                                            <li class="ssc-lw  "><a href="http://www.betfair.com/aboutUs/Rules.and.Regulations/" class="sscmw width(1000) height(600) autoscroll(true) ssc-rulesAndRegulations" target="_popup" title="Regras e Regulamentos" data-gtml="rulesAndRegulations">Regras e Regulamentos</a></li>
                                            <li class="ssc-lw  "><a href="http://www.betfair.com/aboutUs/Terms.and.Conditions/" class="sscmw width(1000) height(600) autoscroll(true) ssc-termsAndConditions" target="_popup" title="Termos e Condições" data-gtml="termsAndConditions">Termos e Condições</a></li>
                                            <li class="ssc-lw responsibleGambling "><a href="http://content.betfair.com/misc/?product=portal&amp;sWhichKey=ResponsibleGambling&amp;brand=ALL_BRANDS&amp;region=GBR&amp;locale=pt" class=" ssc-gamblingCanBeAddictive" target="_blank" title="" data-gtml="gamblingCanBeAddictive">Jogar pode tornar-se num vício. Joga com responsabilidade.</a></li>
                                            <li class="ssc-lw  last"><a href="http://content.betfair.com/misc/?product=portal&amp;sWhichKey=gamCare&amp;locale=pt&amp;region=GBR&amp;brand=betfair&amp;entrydomain=betfair.com" class="sscmw width(1000) height(600) autoscroll(true) ssc-parentalSupervision" target="_popup" title="Supervisão dos pais" data-gtml="parentalSupervision">Supervisão dos pais</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>