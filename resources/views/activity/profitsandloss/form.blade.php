<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <script type="text/javascript">
      //document.domain = "betfair.com";
      var exchangeCount = 1;
    </script>

    <title>G&P em Aposta</title>
    <meta name="description" lang="en" content="A bolsa de apostas Betfair oferece probabilidades melhores e a possibilidade de apostar em eventos desportivos em directo. Aposte a favor ou contra em futebol, corridas de cavalos, críquete, ténis, râguebi, golfe e muitos outros desportos.">
    <meta name="keywords" lang="en" content="Betfair,bolsa de apostas online,probabilidades,futebol,corridas de cavalos,corridas de cães,golfe,críquete,apostas “Em Directo”,râguebi,ténis,probabilidades fixas,desportos motorizados,voleibol,atletismo,boxe,dardos,póquer,bacará,blackjack,texas hold’em,reality shows,big brother,pop idol">
    <meta name="robots" content="index,follow">
    <meta http-equiv="expires" content="-1">
    <meta http-equiv="content-type" charset="UTF-8">
    <link rel="shortcut icon" href="https://cache.ssl.site.sports.betfair.com/favicon.ico">
    <link rel="stylesheet" href="https://cache.ssl.site.sports.betfair.com/exchange-web/resources/betfair/standard/common/content/css/Global-css.jsp" type="text/css">
    <script language="Javascript" src="https://cache.ssl.site.sports.betfair.com/20.0.25.2/js/FormValidation.js"></script>
    <script language="Javascript" src="https://cache.ssl.site.sports.betfair.com/20.0.25.2/user/Utilities.js"></script>
    <script language="Javascript">
      var offset = 1516967092072;
      var oCurrentDate = new Date(Date.UTC(1970,00,01,00,00,00) + offset);

      function changeTimePeriod(sOffset) {
        var oFromDate;
        switch (sOffset) {
          case "3hours" :
            var oFromDate = new Date(oCurrentDate - (1000*60*60*3));
          break;
          case "6hours" :
            var oFromDate = new Date(oCurrentDate - (1000*60*60*6));
          break;
          case "today" :
            var oFromDate = new Date(oCurrentDate.getUTCFullYear(),oCurrentDate.getMonth(),oCurrentDate.getDate(),00,00,00);
          break;
          case "yesterday" :
            var oFromDate = new Date(new Date(oCurrentDate.getUTCFullYear(),oCurrentDate.getMonth(),oCurrentDate.getDate()-1,00,00,00));
          break;
          case "7days" :
            var oFromDate = new Date(new Date(oCurrentDate.getUTCFullYear(),oCurrentDate.getMonth(),oCurrentDate.getDate()-7,00,00,00));
          break;
          case "30days" :
            var oFromDate = new Date(new Date(oCurrentDate.getUTCFullYear(),oCurrentDate.getMonth(),oCurrentDate.getDate()-30,00,00,00));
          break;
          case "3months" :
            var oFromDate = new Date(new Date(oCurrentDate.getUTCFullYear(),oCurrentDate.getMonth()-3,oCurrentDate.getDate(),00,00,00));
          break;
        }
        document.forms[0].startDateString.value = formatDate(new Date(oFromDate.getTime() + (oFromDate.getTimezoneOffset() * 60 * 1000)));
        document.forms[0].endDateString.value = formatDate(new Date(oCurrentDate.getTime() + (oCurrentDate.getTimezoneOffset() * 60 * 1000)));
      }

      function formatDate(oWhichDate) {
        var sReturnDate;
        var sMonth = oWhichDate.getMonth() + 1 + "";
        var sDay = oWhichDate.getDate() + "";
        var sHour = oWhichDate.getHours() + "";
        var sMinute = oWhichDate.getMinutes() + "";
        sReturnDate = oWhichDate.getUTCFullYear() + "-";
        if (sMonth.length==1) {
          sReturnDate += "0";
        }
        sReturnDate += sMonth + "-";
        if (sDay.length==1) {
          sReturnDate += "0";
        }
        sReturnDate += sDay + " ";
        if (sHour.length==1) {
          sReturnDate += "0";
        }
        sReturnDate += sHour + ":";
        if (sMinute.length==1) {
          sReturnDate += "0";
        }
        sReturnDate += sMinute;
        return sReturnDate;
      }

      function validate() {
        var bSubmitFlag = true;
        if (validateFormat(document.forms[0].startDateString.value,"A data de início deve ter o formato aaaa-mm-dd","A data de início deve ter o formato aaaa-mm-dd hh:mm") && validateFormat(document.forms[0].endDateString.value,"A data de fim deve ter o formato aaaa-mm-dd","A data de fim deve ter o formato aaaa-mm-dd hh:mm") && validateDate(document.forms[0].startDateString.value,"A data de início deve ser uma data e hora válidas") && validateDate(document.forms[0].endDateString.value,"A data de fim deve ser uma data e hora válidas")&& validateDateNotInFuture(document.forms[0].startDateString.value,"A data de início não pode ser uma data futura")) {
          var oToDateArray = document.forms[0].endDateString.value.split("-");
          var oToDate = new Date(oToDateArray[0],oToDateArray[1]-1,oToDateArray[2]);
          var oFromDateArray = document.forms[0].startDateString.value.split("-");
          var oFromDate = new Date(oFromDateArray[0],oFromDateArray[1]-1,oFromDateArray[2]);
          var oEarliest3MonthDate = new Date(new Date(oCurrentDate.getUTCFullYear(),oCurrentDate.getMonth()-3,oCurrentDate.getDate(),00,00,00));
          var oEarliest7DayDate = new Date(new Date(oCurrentDate.getUTCFullYear(),oCurrentDate.getMonth(),oCurrentDate.getDate()-7,00,00,00));
          if ((oFromDate - oToDate ) > 0) {
            bSubmitFlag = false;
            alert("A data de fim não pode ser anterior à data de início");
          }
        }
        else {
          bSubmitFlag = false;
        }
        return bSubmitFlag;
      }

      function validateFormat(oWhichInput,sAlertMessage,sAlertMessage2) {
        var bValidFormat = true;
        var sErrorMessage;
        var oFormatArray;
        if (oWhichInput.length>10) {
          oFormatArray = new Array('n','n','n','n','-','n','n','-','n','n',' ','n','n',':','n','n');
          sErrorMessage = sAlertMessage2;
        }
        else {
          oFormatArray = new Array('n','n','n','n','-','n','n','-','n','n');
          sErrorMessage = sAlertMessage;
        }
        for (iCounter=0; iCounter<oFormatArray.length; iCounter++) {
          if (oFormatArray[iCounter]=="n") {
            var sCharAt = oWhichInput.substring(iCounter,iCounter+1);
            if (sCharAt!="0" && sCharAt/sCharAt!=1) {
              bValidFormat = false;
            }
          }
          if (oFormatArray[iCounter]=="-") {
            if (oWhichInput.substring(iCounter,iCounter+1)!="-") {
              bValidFormat = false;
            }
          }
          if (oFormatArray[iCounter]==" ") {
            if (oWhichInput.substring(iCounter,iCounter+1)!=" ") {
              bValidFormat = false;
            }
          }
          if (oFormatArray[iCounter]==":") {
            if (oWhichInput.substring(iCounter,iCounter+1)!=":") {
              bValidFormat = false;
            }
          }
        }
        if (oWhichInput.length>16) {
          bValidFormat = false;
        }
        if (!bValidFormat) {
          alert(sErrorMessage);
        }
        return bValidFormat;
      }

      function validateDate(oWhichInput,sAlertMessage) {
        var bValidDate = true;
        var oValidDate = new Date(oWhichInput.substring(0,4),oWhichInput.substring(5,7)-1,oWhichInput.substring(8,10),oWhichInput.substring(11,13),oWhichInput.substring(14,16),00);
        if ((oValidDate.getUTCFullYear() != oWhichInput.substring(0,4)) || (oValidDate.getMonth() != oWhichInput.substring(5,7)-1) || (oValidDate.getDate() != oWhichInput.substring(8,10)) ||  (oValidDate.getHours() != oWhichInput.substring(11,13)) ||  (oValidDate.getMinutes() != oWhichInput.substring(14,16))) {
          bValidDate = false;
        }
        if (!bValidDate) {
          alert(sAlertMessage);
        }
        return bValidDate;
      }

      function validateDateNotInFuture(oWhichInput,sAlertMessage) {
        var bValidDateNotInFuture = true;
        var oInputDate = new Date(oWhichInput.substring(0,4),oWhichInput.substring(5,7)-1,oWhichInput.substring(8,10),oWhichInput.substring(11,13),oWhichInput.substring(14,16),00);
        if ((oCurrentDate - oInputDate) < 0) {
          bValidDateNotInFuture = false;
        }
        if (!bValidDateNotInFuture) {
          alert(sAlertMessage);
        }
        return bValidDateNotInFuture;
      }

      function disableInput(obj) {
        obj.disabled = !(obj.disabled);
        var z = (obj.disabled) ? 'disabled' : 'enabled';
      }
    </script>
  </head>
<body onload="parent.exchangeLoaded(1);changeTimePeriod('today');" class="GlobalBackground" leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onLoad="changeTimePeriod('today');">
<form action="{{route('formProfitsAndLoss')}}" onsubmit="return validate();" method="post">
  {{ csrf_field() }}


<table border="0" cellspacing="0" cellpadding="0" width="100%">
  <tr>
    <td width="100%">
      <table border="0" cellspacing="0" cellpadding="10" width="100%">
        <tr>
          <td class="GlobalHeadingBackground" width="100%">
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
			  	    <tr>
			  		    <td class="GlobalHeadingBackground" height="20"><span class="GlobalHeadingText">G&P em Aposta : Carteira Principal</span></td>
              </tr>
              <tr>
                <td class="GlobalHeadingBackground" width="100%" height="20">
                  <span class="GlobalHeadingText"><span class="HeaderText"> Augusto Cesar Gomes Queiroz-&nbsp;26-Jan-2018 11:44</span></span><br/><br/>
                </td>
              </tr>
              <tr>
                <td class="GlobalHeadingBackground" width="100%">
                  <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td class="GlobalHeadingBackground">
                        <table border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td align="right"><span class="HeaderText">Período:</span>&nbsp;</td>
                            <td>
                              <select name="timeperiod" onChange="changeTimePeriod(this.value);">
                                <option value="3hours" >Últimas 3 Horas</option>
                                <option value="6hours" >Últimas 6 Horas</option>
                                <option value="today" selected>Só de Hoje</option>
                                <option value="yesterday" >Desde Ontem</option>
                                <option value="7days" >Últimos 7 dias</option>
                                <option value="30days" >Últimos 30 dias</option>
                                <option value="3months" >Últimos 3 Meses</option>
                              </select>
                            </td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                            <td class="HeaderText">(relacionado com a data de fixação do evento)<br><br></td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                            <td>
                              <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td nowrap class="HeaderText"><input type="text" name="startDateString" maxlength="25" size="14"/><br>(yyyy-mm-dd hh:mm)</td>
                                  <td nowrap><span class="HeaderText">a</span>&nbsp;<br><br></td>
                                  <td nowrap class="HeaderText"><input type="text" name="endDateString" maxlength="25" size="14"/><br>(yyyy-mm-dd hh:mm)</td>
                                  <td nowrap>&nbsp;<input id="getPandLButton" type="submit" class="button" value="Baixar G & P"><br><br></td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                          
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>
          
              G&P em Aposta permite-lhe rever as apostas que fez. Especifique o período de tempo em que colocou a aposta e depois introduza os tipos de Mercado e o desporto.<br><br>G&P em Aposta está disponível on-line para os últimos 3 meses. Por favor contacte o balcão de atendimento ao cliente <a href="javascript:if (parent) { parent.getHelp('Help.Contact.Us'); }" class="GlobalHelpLink">&nbsp;?&nbsp;</a> se precisar de consultar os seus G&P em Aposta de há mais tempo.
          
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td width="100%">
      <table id="PLBody" style="display:''" width="100%" cellspacing="1" cellpadding="0">
        <tbody>
          <tr>
            <td width="100%">
              <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                  <tr>
                    <td class="GlobalBackground" width="50%">Corridas de cavalos</td>
                    <?php if(isset($profits)){?>
                    <td class="GlobalBackground" width="50%" nowrap="" align="right">A Exibir <span class="GlobalBoldedText"> 1 - 20 </span> {{count($profits)}} mercados</td>
                    <?php } ?>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <?php if(isset($profits)){?>
            <tr>
              <td width="100%">
                <table width="100%" cellspacing="0" cellpadding="2" border="0">
                  <tbody>
                    <tr align="center">
                      <td class="GlobalTableHeaderBackground" align="left"><span class="GlobalBoldedText">Mercado</span></td>
                      <td class="GlobalTableHeaderBackground"><span class="GlobalBoldedText">Hora de inicio</span></td>
                      <td class="GlobalTableHeaderBackground" nowrap=""><span class="GlobalBoldedText">Data da última resolução</span></td>
                      <td class="GlobalTableHeaderBackground"><span class="GlobalBoldedText">Lucro/prejuízo (€)</span></td>
                    </tr> 
                    @foreach($profits as $profit)
                      <tr align="center">
                        <td class="GlobalTableCell" align="left">
                          <!-- <a href="/reporting/bettingpandl/BettingPandLForMarket.do?marketId=139268705&amp;startDateString=2017-12-27+00%3A00&amp;endDateString=2018-01-26+09%3A09&amp;timeperiod=30days&amp;sportId=7">Corridas de cavalos / ChelmC 25th Jan : 2m hcap</a> -->
                          <a href="/reporting/bettingpandl/BettingPandLForMarket.do?marketId=139268705&amp;startDateString=2017-12-27+00%3A00&amp;endDateString=2018-01-26+09%3A09&amp;timeperiod=30days&amp;sportId=7">
                            {{$profit->market}}
                          </a>
                        </td>
                        <td class="GlobalTableCell" nowrap="">
                          {{$profit->start_timeFormated()}}
                        </td>
                        <td class="GlobalTableCell" nowrap="">
                          {{$profit->resolution_dateFormated()}}
                        </td>
                        <td class="GlobalTableCell">
                          {{$profit->profit_prejudice}}
                        </td>
                      </tr>
                    @endforeach
                    <tr>
                      <td>
                        {{$profits->links()}}
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </td>
  </tr>
</table>
</form>
</body>
</html>
