<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Balance extends Model
{
    protected $table = 'balances';

    public $timestamps = false;

    public function user(){
        return $this->belongsTo('App\Model\User');
    }
}
