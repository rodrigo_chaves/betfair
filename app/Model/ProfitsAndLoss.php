<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ProfitsAndLoss extends Model
{
    protected $table = 'profitandloss';

    public function resolution_dateFormated(){
        $carbon = Carbon::createFromFormat('Y-m-d H:i:s', $this->resolution_date);
        return $carbon->format('d-M-y H:i');
    }

    public function start_timeFormated(){
        $carbon = Carbon::createFromFormat('Y-m-d H:i:s', $this->start_time);
        return $carbon->format('d-M-y H:i');
    }
}
