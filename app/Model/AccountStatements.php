<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class AccountStatements extends Model
{
    protected $table = 'account_statements';

    public function user(){
        return $this->belongsTo('App\Model\User');
    }

    public function account_type(){
        return $this->belongsTo('App\Model\AccountStatementType','account_statement_types_id','id');
    }

    public function placedFormated(){
        $carbon = Carbon::createFromFormat('Y-m-d H:i:s', $this->placed);
        return $carbon->format('Y-m-d H:i');
    }

    public function createdFormated(){
        $carbon = Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at);
        return $carbon->format('Y-m-d H:i');
    }


    /** scopes */
    public function scopeByAccountType($query, $desc){
        return $query->whereHas('account_type',function($query) use ($desc){
            $query->where('description','LIKE',$desc);
        });
    }
}
