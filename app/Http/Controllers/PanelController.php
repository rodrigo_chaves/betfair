<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Balance;
use App\Model\User;

class PanelController extends Controller
{
    public function index(Request $request){
        $user = User::first();

        return View('panel.index')->with('user',$user);
    }
}
