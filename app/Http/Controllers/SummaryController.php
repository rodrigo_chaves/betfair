<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\User;
use App\Model\AccountStatements;

class SummaryController extends Controller
{
    public function accountstatement(){
        $user = User::first();
        return View('summary.accountstatement')->with('user',$user);
    }

    public function form(Request $request){
        // if($request->isMethod('post')){

        // }

        $account_statements = AccountStatements::paginate(20);


        return View('summary.accountstatement.form')->with('account_statements',$account_statements);
    }

    public function filter(Request $request){
        //dd($request->input());
        $account_statements = null;

        if($request->input('include') != 'all'){
            $query = AccountStatements::byAccountType($request->input('include'));

            $account_statements = $query->paginate(20);
        }
        else{
            $account_statements = AccountStatements::paginate(20);
        }

        //$account_statements = AccountStatements

        return View('summary.accountstatement.form')->with('account_statements',$account_statements);
    }
}
