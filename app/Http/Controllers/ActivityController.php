<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\ProfitsAndLoss;
use App\Model\User;

class ActivityController extends Controller
{
    public function profitandloss(Request $request){
        $user = User::first();
        return View('activity.profitandloss')->with('user',$user);

    }

    public function form(Request $request){
        if($request->isMethod('post')){
            $startDate = $request->input('startDateString');
            $endDate = $request->input('endDateString');

            $profitsAndLoss = ProfitsAndLoss::whereBetween('start_time',[$startDate,$endDate])->paginate(20);
            return View('activity.profitsandloss.form')->with('profits',$profitsAndLoss);
        }
        return View('activity.profitsandloss.form');
    }
}
