<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
    
// });


//Route::get('/','PanelController@index');

Route::get('/exchange/plus',function(){
  return view('exchange_plus');
});

Route::get('/',function(){
  return redirect('/exchange/plus');
});

Route::get('/panel','PanelController@index');

Route::prefix('activity')->group(function(){
  Route::get('/profitandloss','ActivityController@profitandloss')->name('profitandloss');
  Route::match(['get','post'],'/form','ActivityController@form')->name('formProfitsAndLoss');
});

Route::prefix('summary')->group(function(){
  Route::get('/accountstatement','SummaryController@accountstatement')->name('account_statement');
  Route::get('/form','SummaryController@form')->name('form_account_statement');
  Route::get('/filter','SummaryController@filter')->name('filter_account_statement');
});